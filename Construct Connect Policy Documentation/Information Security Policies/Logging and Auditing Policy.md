title: Logging and Auditing Policy
author: vCISO
published: 2020-09-03
effective date: 2020-09-03
policy level: Annual General
approver(s): Dana Oney - SVP IT ops., Bob Ven - CTO/CIO
location or team applicability: All locations, all teams
status: draft


# GENERAL

## Objective

The purpose of this policy is to outline logging and auditing mechanisms to both prevent and detect security issues across ConstructConnect by: 

- Identifying systems used to store, manage, or transmit confidential or sensitive information,
- Monitoring identified systems for unintended and/or unauthorized use or access,
- Recording activity of individual processes or users,
- Logging of perimeter access.

## Scope

This policy applies to all employees, contractors, officers, and temporary workers of ConstructConnect. The policy applies to any record that contains personal or other sensitive information in any format and on any media, whether in electronic or paper form to any system used to store, protect, or process said information. 

## Responsibilities

Risk Committee
: Responsible for ensuring the appropriate resource, process, people, and technologies are available to the CISO and IT Teams to enable this policy.

Corporate Information Security Office (CISO)
: Responsible for overall management of the implementation of this policy, including but not limited to selection solutions or methods for logging.

IT Team
: Responsible for the execution, monitoring, and reporting related to system and network logging and alerts, including implementation of the chosen logging solution(s) and ensuring log storage locations are secured, with sufficient storage capacity and backups as necessary.

System and Application Owners
: Responsible for ensuring that the systems and applications they oversee capture all necessary security logs such as authentication events, permissions and configuration changes, etc., sending them to the selected logging solution, coordinating with the IT Team. 

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Configuration

Logging and monitoring procedures must be documented for all information systems. Logging must be enabled on all clients, servers, and other equipment (network routers, switches, etc.) involved in hosting, storing, or processing classified and/or sensitive information to capture a picture of what is happening on individual devices including servers and applications. 

## Events

Logs should include at least the following:

- Dates and times of key actions (e.g., log on, log off, configuration state change),
- Successful and unsuccessful system access attempts,
- Successful and unsuccessful data and other resource access attempts,
- Changes to system parameters and configurations,
- Use of system utilities and applications, and
- Escalation of privileges.

## Access

Monitoring and alerting must be configured to detect and notify the IT Team whenever there is unintended and/or unauthorized access, including:

- Any unauthorized access attempts,
- Unusual use of privileged accounts,
- Attempted log detection/modification,
- Attachment of unauthorized removable media devices, and
- Unusual patterns of activity (time, system changes, etc.).

## Errors

Fault or error logging will be enabled on all systems and applications dealing with classified information and all reported faults will be investigated to ensure that security controls have not been compromised. Faults should be logged and investigated according to ConstructConnects information response processes. 

# Review and Audits

The contents of system logs will be reviewed on a regular basis and the frequency must be determined according to the following factors:

- Business criticality of the application,
- Classification of the data assets involved,
- Frequency with which systems have been attacked or compromised previously
- Level of exposure to external networks

# Log Protection

Log data must be retained for a period of one year. To accomplish this, log data must be protected through a strict permissions model following the principle of least privilege to ensure that log files cannot be altered or deleted. Backups of logs must be taken, and where possible, key events from log files must be copied to a centralized location and archived. 

For any system containing ConstructConnect classified information, logs of all administrator and operator activities must be collected so that it is possible to identify the actions that were carried out under such user accounts. 

# REFERENCES

## Regulations.

- PCI-DSS

## NIST CSF

- Detect
- Respond

## Roper CIS Controls

- Audit Logging and Monitoring
- Network Security
- Data Protection
- Secure System Development - SDLC
- Incident Response

## Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program
- Policies
    - Risk Management Policy
    - Secure System Development Policy

# Insert any procedure or process documentation (optional):

