title: Risk Management Policy
author: VerSprite vCISO
published: 2020-06-03

#### Policy name:
Risk Management Policy

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
CTO (Bob Ven)    
EVP Finance (Buck Brody)

#### Location or Team Applicability
All locations, all teams

#### Effective date:
input date using YYYY-MM-DD format

#### Policy text:

##### GENERAL

###### Objective

This Risk Management Plan provides information on the measures used to: develop a framework to manage information security risk specifying the criteria for identification, assessment, and treatment; establish risk management roles and responsibilities; and ensure that appropriate technical, administrative and physical controls are implemented.

This plan is driven by many factors with the key factor being risk, and sets the foundation under which ConstructConnect operates and safeguards its data and systems to reasonably reduce risk and minimize the effect of potential incidents. Effective security is a team effort involving the participation and support of all users who interact with data and systems.

###### Scope
This policy applies to all information and information systems at ConstructConnect and all employees who have access to information.

###### Responsibilities
**Steering Committee** is responsible for providing the strategic view for the risk management framework, including the context, environment, strategy and risk response criteria. The Steering Committee must approve risk treatment plans and determine whether the residual risk is acceptable.

**Corporate Information Security Office (CISO)** is responsible for:

- Development, implementation and maintenance of the Risk Management Policy and Risk Management Process;
- Development, implementation and maintenance of all associated standards and guidelines;
- Conducting risk assessments;
- Maintaining the Risk Register; and
- Maintaining the Incident Register.

**Managers** are responsible for ensuring that the Risk Management Policy and Risk Management Process are properly communicated and understood within their respective organizational units.


###### Review
This policy must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CIO owns this Policy, but any update or change must be approved by the [Information Security Committee/ Steering Committee?].

##### DEFINITIONS

- **Impact** is the potential losses associated with an identified event.
- **Likelihood** is the measure of probability of the occurrence of a specific event.
- **Risk** is the combined measurement of Impact and Likelihood.
- **Inherent Risk** is the severity of risk that exists in the absence of any controls.
- **Residual risk** is the severity of risk that remains after controls are accounted for.

##### RISK IDENTIFICATION

Company must identify risks through:

- Regularly conducted risk assessments to identify and assess risks and vulnerabilities that affect the confidentiality, integrity and availability of its data and information systems;
- Reviewing and analyzing incident reports; and
- User reports.

##### RISK EVALUATION

Risks must be evaluated using the combination of likelihood and impact in order to develop a formal plan and prioritize efforts for elimination or mitigation of vulnerabilities. The Risk Evaluation process begins with the identification and evaluation of the impact and likelihood of inherent risk.


Impact can be evaluated using a scale, for example:


| Impact Rating | Description |
| --- | --- |
| High Impact | Potential for total compromise of systems and network infrastructure, access to sensitive or confidential information, personal information data breaches, etc. |
| Medium Impact | Partial compromise of systems and network infrastructure, data breaches including partial personal information or a limited number of records, etc. |
| Low Impact | Access to non-sensitive company information, data breaches involving only unidentifiable personal information, etc. |


Likelihood can also be evaluated using a scale, for example:

| Likelihood Rating | Description |
| --- | --- |
| High Likelihood | It occurs frequently. |
| Medium Likelihood | It happened in the past and it may occur again. |
| Low Likelihood | It could occur, but it is uncommon. |


The combination of Impact and Likelihood is combined to determine the overall Risk Rating.

| RISK RATING | High Likelihood | Medium Likelihood | Low Likelihood |
| --- | --- | --- | --- |
| **High Impact** | Critical | High | Medium |
| **Medium Impact** | High | Medium | Low |
| **Low Impact** | Medium | Low | Low |

###### Mitigating Controls
Once inherent risks are identified and evaluated, all technical, administrative and/or physical controls reducing the impact or likelihood of the identified risks must be identified and evaluated for effectiveness. Mitigating controls may:

- **Prevent** Controls that reduce the impact or likelihood of risk by not allowing the risk even to occur (e.g. complex passwords, approval processes, door locks);
- **Detect** Controls that detect risk events after or while they occur, thereby reducing the impact of the event (e.g. logging and monitoring, audits, motion sensors); and/or
- **Recover** Controls that facilitate the recovery from risk events after they occure (e.g. backups, continuity strategy, asset or personnel redundancy).

The residual risk is evaluated by accounting for the effectiveness of the controls at mitigating the inherent likelihood and/or impact of identified risks. The residual risk is then rated according to the new likelihood and/or impact, if any.

##### RISK TREATMENT

A plan for risk treatment must be proposed for all identified risks, regardless of risk rating. Risk Treatments include:

- **Avoid** Eliminating the risk by not allowing actions that would cause the risk to occur, usually by eliminating use of the people, process, service, or technology that is at risk;
- **Mitigate** - Selecting and implementing controls to lower the risk, usually by adopting security or operational controls as defined by a framework such as COBIT, ISO 27000, or NIST to lessen the likelihood and/or impact of an identified risk;
- **Transfer** - Sharing the associated risks with other parties, for example insurers or suppliers; and
- **Accept** - Documenting the risk and criteria used to determine that the risk is acceptable and will go without further remediation.

##### APPROVAL

The documented risk rating, mitigating control(s), and plan for risk treatment must be presented to the Steering Committee or its designee. The Steering Committee review the proposed risk treatment plan reduces trisk to an acceptable level in accordance with the Company's risk appetite. The Steering Committee or its designee must then:

- determine that the proposed risk treatment plan reduces risk to an acceptable level and document their approval of the plan and the acceptance of the residual risk, if any; or
- determine that the proposed risk treatment plan is not acceptable an refer the proposed plan back for another iteration of risk identification, evaluation, and treatment.

Any accepted risk must be documented in the risk register.


###### EXCEPTION MANAGEMENT


Exceptions are used to document risks or policy violations that, for one reason or another, cannot be effectively mitigated or remediated. To create a risk exception:

- The exception must be documented, including the rationale for creating an exception as opposed to using a mitigating control or other risk treatment.
- The exception must be validated in writing by the IT Team and the affected business unit leader(s); and
- The exception must be approved by the Steering Committee.

After any exception is approved, the underlying risk must be monitored and regularly reviewed. All critical risks must be reviewed every six months. All high risks must be reviewed every year. All other exceptions must be reviewed every other year.

##### REFERENCES

###### Relevant Regulations.
- PCI-DSS

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).
- Identify


###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).
- 1.1, 1.4, 17.1


###### Related Policies, Plans, Procedures and Guidelines.
- Written Information Security Program
- Access Control Policy
- Vendor Risk Policy


#### Insert any procedure or process documentation (optional):

insert text
