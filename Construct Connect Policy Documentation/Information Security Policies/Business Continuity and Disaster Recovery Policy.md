title: Business Continuity and Disaster Recovery Policy 
author: VerSprite vCISO
published: 2020-06-03

#### Policy name:
Business Continuity and Disaster Recovery Policy

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
Input approver name and title

#### Location or Team Applicability
All locations, all teams

#### Effective date:
input date using YYYY-MM-DD format

#### Policy text:

##### General

###### Introduction

Business operations face many types of events that can partially or completely interrupt their operations resulting in loss of life, revenue loss, reputational harm, and legal liability. To prepare for these issues and attempt to limit or eliminate the impact from these events, Company must adopt policies, standards and guidelines used to ensure resilience and proper risk management. Two critical functions used to address these events are:

- Business Continuity Plans (BCP) - procedural documents providing guidance on how to resume or continue business despite disruption of processes that are critically important to successful business activities; and
- Disaster Recovery Plans (DRP) - procedural documents providing guidance on the tools, assets, and methods used to recover assets, systems and data following natural and/or man-made disasters (i.e. data center outage, flood, pandemic, tornadoes, earthquake, terrorist attacks, etc.).

###### Scope
This policy applies to IT systems which store, process, or transmit information on Company or customer facing networks, as well as critical processes, procedures and personnel.

###### Implementation
The purpose of this Business Continuity & Disaster Recovery Policy is to:

- Safeguard Company's employee's lives and company property;
- Minimize service disruptions for Company's critical services and clients;
- Minimize negative business impacts to Company, including reputation harm, legal liability and financial loss; and
- Ensure timely resumption of critical business operations and IT systems in the event of a business disruption;
The purpose will be achieved by:
- Ensuring that the appropriate resource, processes, people, and technology are deployed to enable this policy and its associated plans;
- Taking necessary preparations to execute BC/DR Plans, including:
 - Conducting a business impact analysis (BIA) to prioritizing assets or processes;
 - Storing and securing adequate backup materials off-site;
 - Providing information and procedures necessary to respond and recover; and
 - Performing adequate cross-training to reduce reliance on key personnel;
- Documenting, maintaining and distributing (or making available) up-to-date BC/DR plans to all relevant parties;
- Reviewing, maintaining, and updating of this policy and its associated plans;
- Coordinating tests of the BC/DR plans;
- Conducting Post Mortems after any activation of any BC/DR plan(s), including tests;
- Modifying the BC/DR Plans as needed based on the results of Post Mortem analysis.

###### Roles & Responsibilities
The following individuals and/or teams are responsible for compliance and enforcement of this policy and its guidelines:

- **Steering Committee** is responsible for ensuring the appropriate resource, process, people, and technology are deployed to enable this policy.
- **Business Continuity Planning Committee** is responsible for:
    - Coordinating the Business Impact Analysis (BIA) process;
    - Maintenance, Review, and approval of the Business Continuity Plan;
    - Distribution of up-to-date Business Continuity (BC) and Disaster Recovery (DR) Plans; and
    - Plan and review results of Business Continuity Plan testing;
- **Disaster Recovery Planning Committee** consists of Company's IT team led by the VP of IT Operations and is responsible for:
    - Ensuring that backup systems, redundancy and alternative processing is configured so that the Recovery Time Objective and Recovery Point Objectives can be met according to criticality of the affected asset;
    - Regular full testing of backup systems, redundancy and alternative processing is carried out and to report the results along with recommendations for improvement to senior IT management; and
    - Creating detailed system, process and application restoration procedures in alignment with the agreed business criticality designation.
    - Disaster Recovery Plan maintenance and review.
- **Emergency Preparedness Team** is responsible for:
    - Managing Company's response in the event of a significant business disruption.
    - Supporting communication with employees, clients, partners and suppliers during significant business disruptions.

##### DEFINITIONS
**Disaster**: any natural, man-made event resulting in:
- Unavailability of any office or data center asset owned or operated by ConstructConnect (including lost access or destruction);
- Loss of, or major disruption to, the computer systems, applications, data, and resources required to perform the operations of the business; and/or
- Impact on employees in key positions who perform critical business operations.
**Recovery Time Objective**: The time it takes to restore service following a service disruption event.
**Recovery Point Objective**: The time between the most recent backup and disruption event, i.e. the measure of time for which data is lost when restoring from backups
**Emergency Preparedness**: The intended response and readiness in an event of a disaster and emergency. This also involves life safety to protect employees and guests located at Company's facilities during a disaster, and can include medical emergencies, shelter in place, and emergency evacuations.
##### DR DISCOVERY ASSESSMENT AND ANALYSIS
To create effective Disaster Recovery Plans, ConstructConnect must perform a DR Assessment and Analysis to identify the following:
1. RTO and RPO;
1. System Components;
1. System Interface;
    1. System Inputs;
    1. System Outputs;
1. Production Environment;
1. DR Environment; and
1. Recovery Process.

##### BUSINESS IMPACT ANALYSIS (BIA)
To create effective Business Continuity Plans, ConstructConnect must identify business processes, determine criticality of those processes and prioritize recovery accordingly. Note that different disaster scenarios may have significantly different impacts on business processes or critical systems.
The following steps should be taken during the BIA process:

1. Schedule and perform BIA interviews with each business process leaders.
1. Document and approve results;
1. Review and complete a risk assessment summary; and
1. Present results and findings to management.

##### IDENTIFYING PROCESSES

All Management, Asset Owners and Department Leads must participate in facilitation sessions to identify and elaborate on business processes at Company. In documenting processes, include the following:

- **Business Service**: By name or definition, identify the specific services to be recovered. In cases where applications — rather than services — may be the recovery target, name the application, identify it as such, and capture the application dependencies.
- **Service Description**: Summarize the services outlined in enough detail that the key stakeholders — from IT and the business — will commonly understand the scope of the services identified.
- **Key Systems to Be Recovered**: Identify which systems and applications are part of the identified services and thus, part of the recovery plan. This includes identifying and mapping all system, application, connectivity, and data dependencies.
- **Key Business Stakeholders**: List the assigned recovery owner by title and contact details.
- **Recovery Procedure**: Document the steps needed to recover a system in the event of common failure scenarios or restore a system from backup.

###### DETERMINE CRITICALITY
Critical processes must be supported in the event of an unplanned interruption to normal operations. Processes should be categorized into varying tiers of criticality to aid in prioritizing recovery and determine minimal acceptable levels of operation for the Company. In determining criticality, consider the following:
- **Operational Considerations**: Identify the operational issues and potential impacts that should be understood if the named services are lost. This should include both business and technical operations. Technical details of redundant assets and fail-over functionality must be included.
- **Business Impact**: Where possible, quantify the level, severity and cost of the impact on the core business objectives (that is, dollars per hour of downtime, service impact or loss of customer confidence, health and safety, etc.);
- **IT Disaster Recovery Leads**: List the IT people assigned as the recovery owner of specified services specifying title.
- ****RTO and RPO Expectations**: Indicate the recovery service expectations for each targeted service and application platforms. These service levels are generally a function of balancing risk mitigation and recovery costs; and
###### Criticality Rating
The below table shows a tiering system for prioritizing recovery efforts:

| Tier | Criticality | Business Process | Business Criticality Measure | RPO | RTO |
| --- | --- | --- | --- | --- | --- |
| Tier 0 | Foundational | Support infrastructure for critical business systems | Critical systems dependent on infrastructure (networking, DNS, enterprise directory) | Must be recovered with or prior to mission-critical services | Must be recovered with or prior to mission-critical services |
| Tier 1 | Mission Critical | Critical business function and/or revenue impacting, generally customer facing | Critical services impacted and/or revenue stops if systems are lost | Less than 1 hour | Less than 1 hour |
| Tier 2 | Critical | Internal operations and/or customer relationships focused | Services continue though data collection and transaction processing may be delay. Operational workarounds exist | 1 to 4 hours | 1 to 4 hours |
| Tier 3 | Important | Internal operation with alternative method | Support services that can fall back to manual execution - business critical activities may be slowed | 4 to 48 hours | 4 to 24 hours |
| Tier 4 | Noncritical | Experimental or Internal only systems | Systems of automation, experimentation and promotion | More than 48 hours | More than 24 hours |

*Note that Tier 1 RTO and RPO are currently aspirational.*
###### BASELINE PLANNING REQUIREMENTS
ConstructConnect maintains offices in several locations. Should a facility(s) become unavailable, the company employees will continue their business functions remotely. In order for employees to continue daily business operations remotely, ConstructConnect must ensure the following:

- Company laptops, networks, and relevant applications and updated and configured; and
- Ensure employees have appropriate access to systems and applications required to continue operations;

##### PLAN ACTIVATION

Should an event that disrupts normal business operations and impacts essential operations of Company, the following steps should be followed to activate the Business Continuity and/or Disaster Recovery plans.
**NOTE: If your department, team, or location cannot operate and/or there is an emergency life safety issue, go directly to Evacuation Procedures located in the [Emergency Procedure Manual].**

###### Incident or Event Identification

The individual or team identifying an event must immediately notify the [VP of IT Operations] and/or designee per the BC/DR contact list for validation. The VP of IT Operations and/or designee will assess the situation and activate the appropriate BC/DR plan and contact the Steering Committee or its designee as soon as possible using the contact information outlined in [insert appendix here] of this policy.

###### Notification

Depending on the scope of the event, employees, partners, clients and service providers may need to be notified regarding details of the event, such as service disruption, impact on physical facility, recovery progress, and/ or links to any particular recovery procedures. Please see Company's communication plan located in the [DR Run Book].
###### Emergency Contact List
ConstructConnect maintains a company wide Emergency Contact List that contains the email address and phone number for Company employees and contractors. This list also includes the contact information, if available, for Company's mission critical suppliers and systems.

The Emergency Contact List serves the dual purpose to:
- Ensure that all employees and, if applicable, suppliers receive updates during significant business disruptions; and
- Provide up-to-date contact information in case of system-wide IT or email outages.

Each member of the Emergency Preparedness Team maintains a copy of the Emergency Contact List. Steering Committee or its designee reviews and updates the Emergency Contact List on at least an annual basis.

###### Recovery Procedures

Recovery procedures must be created for all business processes and systems to align with the criticality of the business system(s) and/or process(es). The documentation should contain full detail of the system, database, process and inter-connectivity including, but not limited to, vendor, hardware/software requirements, configurations, licensing, support options, ownership, testing and official sign-off as is applicable.

###### Data Backup Procedures

During an unexpected service disruption, ConstructConnect must protect client and proprietary data from loss or corruption. To accomplish this, Company:

- have a designated primary and secondary data center in different geographical locations
- performs the following backups of all client data stored in assets, both in production and test environments;
- incremental/differential backups daily;
- full backups weekly;

##### DATA SAFETY AND RECOVERY INITIATIVES

Beyond the safety and security of data in a disaster, initiatives must also be in place for enabling continued business operations from an alternative/temporary location. Consider the following:
1. In the event of a disaster, is ConstructConnect viably able to safeguard all available data and supporting information systems resources? This would include data in electronic/digital and hard copy format.
1. If data and supporting information resources can be effectively safeguarded or rescued immediately before, during, or after a disaster, then authorized personnel are to undertake such steps as necessary.
Following a significant business disruption, ConstructConnect must immediately assess the extent to which the disruption corrupted or lost of any of the critical production data. Authorized personnel are to mobilize accordingly and assess the viability of current data, its integrity, and the types of resources available for helping restore the organization's business operations.
This may include any number of measures, such as the following:

- Using any and/or all existing data that was successfully safeguarded and/or rescued.
- Repairing data that may have been rescued but was also damaged during the disaster.
- Obtaining backup data sets from any number of external resources that are currently being used.
- Completely restore (or to the fullest extent possible) data to a post-disaster platform for ensuring continuance of operations for the organization.
- Ensuring data backup procedures are in place for the post-disaster platform itself.
- Assist and implement any other procedures to ensure the confidentiality, integrity, and availability (CIA) of data and supporting information systems resources.

##### AWARENESS & AVAILABILITY

The BCDRP and related plans must be distributed or made available to all relevant employees for awareness located in BitBucket for online access. Every team should also have a hard copy of the BCDRP.

##### TESTING

In addition to the reviews of this policy on an annual basis, ConstructConnect must test the BC/DR plans against mock disaster or disruption scenarios to ensure that the plans work as intended. Tests may include tabletop exercises, which take the form of a moderated discussion over specific scenarios.
ConstructConnect must generate and retain documentation of these tests, including any identified gaps and action plans. Gaps and their respective remedial action plans should be addressed in accordance with the criticality of the processes they relate to, for example, a gap affecting a tier 0 or 1 issue that could impact the ability to recover a system or process should be prioritized.
Tests of Business Continuity Plans should include:

- Clearly defined objectives for the test;
- A high-level description of the scenario, inclusive of stated assumptions;
- Instructions for all participants in the test; and
- Post-assessment evaluation of gaps identified.

Tests of Disaster Recovery Plans should include:
- Clearly defined objectives for the test;
- A high-level description of the scenario, inclusive of stated assumptions;
- Instructions for all participants in the test;
- Ensure systems/applications recover within their assigned RTO/RPO; and
- Post-assessment evaluation of gaps identified.
- Documenting the total time of each recovery task performed (how long did each recovery task take to execute)

###### Testing Cadence

Business Continuity tests should occur at least once a year and cover, at a minimum, emergency preparedness, life safety, work from home strategies, evacuation drills and service resumption.
Annual Testing Requirement:

- Table-top testing

Disaster recovery tests should occur annually and cover, at a minimum, IT systems recovery, fail over, fail back and backup stability.

Annual Testing Requirement:

- DR failover/failback

##### TRAINING

During tests or a downtime-triggering event, if ConstructConnect identifies gaps relating to skills or familiarity with procedures, training should be arranged for team members supporting different parts of this policy.
The Business Continuity and Disaster Recovery Steering team are responsible for coordination and oversight of this training activity.

##### PROGRAM REVIEW

ConstructConnect shall review this Business Continuity and Disaster Recovery Policy and the security measures defined here in at least annually, or whenever there is a material change in Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of records containing personal or other sensitive information. Such a change may include but is not limited to:

- **Significant Organizational Structure Changes**. Organizational changes such as restructuring departments, key leadership changes, or loss/change of key personnel.
- **Relocation of Resources.** IT systems or other critical services moving to a new geographic location.
- **Changes in Business Processes.** Business processes that change to include or exclude IT systems, handle new data or involve new stakeholders.
- **Changes in BCP Relevant Staff.** Changes to team members intricately involved in the business continuity plan or critical recovery procedures.
- **Changes in Vendors.** New or replacement vendors being introduced to Company that may have different escalation/contact procedures, interdependence, or deployment models.
- **New Computer or IT Systems.** New IT systems that support essential business processes may change the way that recovery procedures work to resume business operations.
- **After Downtime Triggering Event Occurs.** Following any qualifying event, a postmortem should be conducted by the Steering Committee to identify gaps, understand what, if anything could have been prevented, and how training for Company personnel can be improved for the future.

Company shall retain documentation regarding any such program review, including any identified gaps and action plans.

##### ENFORCEMENT

Violations of this Business Continuity and Disaster Recovery Policy will result in disciplinary action, in accordance with Company's information security policies and procedures and human resources policies.

##### EXCEPTIONS

Exceptions to this policy require the approval of the Risk Committee as described in the *Risk Management Policy*.

##### REFERENCES

###### Relevant Regulations.
- PCI-DSS v3.2.1

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).
- Identify
- Respond
- Recover

###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).
- 1.1, 1.2, 1.3, 2.5, 10.1, 10.2, 14.1, 16.1

###### Related Policies, Plans, Procedures and Guidelines.
- Written Information Security Program
- Emergency Procedure Manual
- DR Run Book
- Pipeline DR Plan
- IT Platform Risk View
- Cloud Disaster Recovery

#### Insert any procedure or process documentation (optional):
