title: Asset Management Policy
author: vCISO
published: 2020-09-03
effective date:
status: draft
policy level: Annual General
approver(s): Dana Oney - SVP IT Ops.
location or team applicability: All locations, all teams


# GENERAL

## Objective

The purpose of this policy is to outline requirements for creating inventory, ownership, and valuation of IT assets. This is inclusive of labeling and handling standards and disposal of inventoried assets. 

## Scope

This policy applies to information systems (virtual, physical, computing endpoints, software) that may store, process, or transmit information on ConstructConnect internal, corporate, or cloud-based network environments. 

## Responsibilities

Risk Committee
: is responsible for ensuring the appropriate resources, process, people, and technology are deployed to enable this policy.

Corporate Information Security Office (CISO)
: is responsible for designing, managing, and monitoring security controls as well as auditing compliance with this policy.

Managers
: are responsible for ensuring that personnel under their purview protect information in accordance with this policy. 

IT Team
: is responsible for implementing security controls and IT management controls in alignment with the requirements outlined in this policy.

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Asset Inventory

- Develop process and procedures to create and maintain an asset inventory. The inventory should be organized by the major asset types specified previously in this policy.
- The inventory list should contain the necessary fields to identify, categorize, locate, track, and specify ownership of the asset. This may include but is not limited to: 
    - Unique ID
    - (Storage) location
    - Person or department responsible
    - Information classification
    - Purpose
    - Environment (QA, Prod)

# Network Diagram and Data Flow Mapping

Network maps should be documented and kept current to include both the physical and logical components as much as feasible, this should include servers and applications/services.  The communication and data flow are critical in understanding where critical data resides, as well as how it flows in and out of network.  When defining the sensitive data to the organization, also assess the confidentiality, integrity and availability risks.  Defining the specific risks applicable to the sensitive data will help drive prioritization of cybersecurity efforts, this can be done as part of the network maps or in supporting documentation.

# Asset Ownership

- Ensure each asset is assigned to a business owner who is ultimately responsible for the asset. The owner must ensure the asset is appropriately classified and access restrictions are periodically reviewed to verify alignment with security requirements for a classification.
- Where appropriate, a custodian may be assigned who is responsible for the routine care of the asset. Custodians must ensure identified controls are implemented and maintained.

# Asset Valuation

- Asset business owners must define the criticality of the asset to the business. The Data Management Policy, statutory and regulatory requirements, and organization security policies are combined with the business value to determine the information protection needs of the asset.
- Create a rating process to quantify the value of confidentiality, integrity, and availability required by the assets and translate these values to a composite asset value. 

# Asset Labeling and Handling

- Information Assets must be classified and labeled accordingly to its sensitivity. Data Management Policy provides additional requirements and guidelines regarding the information classification.
- Ensure asset handling criteria is defined to provide the appropriate level of protection to the asset. Data Management Policy provides additional requirements and guidelines regarding the asset handling.
- Ensure acceptable use criteria is defined and communicated to the relevant constituents. Acceptable Use Policy provides specific requirements and guidelines regarding the acceptable use of assets.

# Asset Disposal

When a system is no longer in use, when it’s reached end-of-life status, or if it has been damaged in some way it should be securely disposed of. Secure disposal includes wiping and then subsequently physically destroying the storage media on the device. From this point, the device components can be recycled appropriately. 

In the context of cloud services and cloud-based infrastructure, the vendor provided solution for securely decommissioning an asset should be utilized. 

# Unauthorized Asset Prevention

Ensure that unauthorized assets/software are either removed from the network, quarantined or the inventory is updated  in  a  timely  manner. To  further  protect  the  environment,  implementing application allow lists, network-layer access controls, or utilizing port-level access control, following 802.11x standards, would be the best approach.

# REFERENCES

## Relevant Regulations.

- PCI-DSS

## Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).

- Identify
- Protect

## Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).

1. Cybersecurity Governance and Risk Management
1. Inventory and Asset Management

## Related Policies, Plans, Procedures and Guidelines.

- Written Information Security Program
- Policies
  - Acceptable Use Policy
  - Data Management Policy Policy
