title: Data Management Policy
author: VerSprite vCISO
published: 2020-06-03

#### Policy name:
Data Management Policy

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
CTO (Bob Ven)    
EVP Finance (Buck Brody)

#### Location or Team Applicability
All locations, all teams

#### Effective date:
input date using YYYY-MM-DD format

#### Policy text:
##### RESPONSIBILITIES

**Steering Committee** is responsible for ensuring the appropriate resource, process, people, and technology are deployed to enable this policy.

**The Corporate Information Security Office (CISO)** is responsible for:
- Designing, managing, and monitoring security controls; and
- Auditing compliance with this policy in accordance with the ConstructConnect Information Security Policies and to report the results along with recommendations for improvement to the Steering Committee.

**IT Team** is responsible for implementing technical controls for storing, backing up, tagging, and wiping information stored in digital assets.

**Managers** are responsible for ensuring that the personnel under their authority protects information in accordance with the information security policies related to their area of expertise.

**Employees** are responsible for knowing the information security responsibilities of their job and position.

##### DATA CLASSIFICATION
Data shall be classified according to the following classification scheme:

###### **CONFIDENTIAL**
Information that is extremely sensitive in nature and may not be shared excepted when deemed absolutely necessary for continuation of business, such as:

- **Access Information.** Information used to access Company assets, such as usernames, passwords, 2FA/MFA information, encryption keys, and personal codes.
- **Employee Information.** Any non-public information Company possesses about its employees, including compensation data, performance ratings, health information, benefits information, and other information required to be retained by Human Resources.
- **Customer Information.** Any personal information or personally identifiable information about a customer, meaning information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual.
- **Proprietary Business Information.** Trade secrets, source code.

###### SENSITIVE
Information that, if disclosed, could violate the privacy of individuals or customers, reduce the company's competitive advantage or cause significant financial damage to its external business partners and/or customers and may only be released with the permission of the data custodian on a “need-to-know” basis, such as:

- **Other Business Information.** Any non-public information about Company, its respective business operations, customer or prospect lists, pricing information, business plans, or business and financial results, including information that others have provided to Company and Company has promised to keep confidential.
- **Other IT Information.** IT and security documentation which is not related to passwords or any type of access. For example, Network diagrams, data flow diagrams, configurations, vulnerability assessments, etc.

###### INTERNAL
Information available to all employees, but requires a non-disclosure agreement before providing access to third parties, such as:

- **Policies.** Company guidelines or instructions for employees including announcements, HR policies, and security controls.
- **Employee Information.** Non-personal information about employees not intended for public consumption, such as business e-mail addresses or phone numbers.
- **Internal Announcements.** Any communications circulated inside the company but not shared with outside partners or the media.
- **Training Materials.** Including manuals, videos, etc.

###### PUBLIC
Information that has been made available for public distribution through authorized company channels. Only authorized employees should distribute public information, such as:

- Press Releases,
- Marketing Materials,
- Jobs Posted to External Sites,
- Website Data, and
- Published Financial Results.

##### DATA RETENTION
The language below defines the minimum data retention requirements for different classifications of data.

| Type of Data | Digital Retention Period | Physical Retention Period |
| --- | --- | --- |
| Corporate Data | Lifetime of Enterprise | N/A |
| Employee E-mail | 2 years | N/A |
| Client Contact Data | 7 years for inactive clients | 5 years for inactive clients |
| Consumer Data | 6 years | Not to be stored |
| Legal Documents | 6 years | 6 years |
| Tax Records | 6 years | 6 years |
| HR Documents | 6 years | 6 years |
| All Other | 6 years, assuming no access within past 3. | 6 years |

**IMPORTANT** - If an employee believes, or the Legal Department advises, that specific records are relevant to litigation or potential litigation, those records must be protected until the Legal Department determines that the records are no longer needed and that determination is formally communicated. This exception supersedes any other retention period or destruction requirement established in this Data Retention Policy. If any employee believes this exception may apply, or has any question regarding this exception, please contact the Legal Department.

###### Encryption
In storage and transmission, encryption should be used according to the classification of the data. The table below provides general guidance about the requirement for encryption:
| Type of Data | Encryption in Transit | Encryption at Rest |
| --- | --- | --- |
| Confidential | Required for internal and external communication | Required |
| Sensitive | Required for external communication | Required |
| Internal | Required for external communication | Not required |
| Public | Not required | Not required |

###### Disposal
Data must be removed as soon as possible following the end of the appropriate Retention Period. This removal must be verified by the manager of the employee removing the information.

IT systems that have been used to process, store, or transmit confidential or sensitive information must not be released from Company's control until the equipment has been sanitized and all stored information has been cleared.

Digital/ Electronic Data classified as Sensitive or Confidential must be wiped using appropriate secure deletion programs employing methods with, at minimum, seven pass erasure. Recommended method is US DoD 5220.22-M (8-306. /E, C & E) standard.

Physical copies with confidential and sensitive data must be properly disposed of by placing it in locked shredding bins for the destruction vendor to come onsite and crosscut shred.

###### Enforcement
Violations of this Data Classification Policy will result in disciplinary action, in accordance with information security and human resources policies and procedures.

#### REFERENCES

###### Relevant Regulations

- PCI-DSS
- US data privacy laws

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).]

- Identify

###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).
- 1.3, 8.6, 10.2, 11.1

###### Related Policies, Plans, Procedures and Guidelines.
- Written Information Security Program
- Access Control Policy
