title: Fixed Asset Policy  
published: 2020-07-16  
author: Eric Bodge, Assistant Controller  
policy level: important  
approvers: Dave Storer, Controller  
applies to: Accounting Team  
effective date: 2020-07-09  


This policy provides operational guidance that serves as an extension of
the Roper Accounting Manual (RAM). If any below guidance becomes in
conflict with RAM, RAM shall take precedence.

A fixed asset must have one unit of property that:

1. has an economic useful life that extends beyond 12 months; and
1. was acquired or produced for a cost of $5,000 (in local currency) or more (with one exception noted below).
	- *One exception to the $5,000 minimum threshold are laptop computers, which will always be capitalized to provide additional tracking and safeguarding within the accounting subledger.*

All fixed assets require a Capital Expenditure Request (CER) completed
with the necessary approvals prior to purchase. Accounting is
responsible to track spend on the CER and maintain a link back to the
CER within the subledger. Preference is to aggregate purchases under
each CER together as one asset for each category (ex. a building CER
with a single furniture and a single leasehold improvement).

A Construction In Progress (CIP) account is used to account for in
process fixed asset. Fixed assets are capitalized and depreciated within
the subledger within the same calendar quarter when each asset is ready
for its intended use. Any purchases that remain in CIP for greater than
six months should be reviewed for capitalization or expense.

Useful lives shall be as follows (in months):

- Computer & Equipment- 36
- Furniture & Fixtures- 60
- Leasehold Improvements- Length of the lease (max 60)
- Software- 36

Upon capitalization, the accounting team will assign a unique internal
tracking number that should also contain additional description
information (ex. serial number, location, count of chairs) to locate the
asset during a physical inventory.

All disposals of capital assets require a Capital Disposal Request (CDR)
completed with the necessary approvals. Accounting is responsible for
reviewing all assets in the subledger for impairment or disposal at
least annually.

*Accounting is responsible for conducting a full physical inventory at
least once every two years. Serialized items should be counted
specifically. Non-serialized items, such as furniture, should be
compared back to the quantity initially purchased or last inventoried. A
CDR should be prepared to impair / dispose of missing or non-functioning
assets accordingly.*


