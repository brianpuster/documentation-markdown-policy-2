title: Custom Development Policy
author: Bob Ven - CTO
published: 2019-09-10


#### Policy level: 
Very Important 
 
#### Policy Author: 
Bob Ven
 
#### Policy Approvers:
CXO; CTO; EVP Finance
 
#### Applicable Locations:
All locations, all teams 
 
#### Effective Date: 
2019-08-15 
 
#### Policy text: 
No custom software development is to be included as part of a customer’s subscription or license agreement (new or renewal), or as part of an independent arrangement.  The following types of activity are all considered custom development:

- Modifications to base product code with special code branches created by customer
- Data transformation or ETL services to enrich, transform or modify data that requires any resources from Back Office or Development DBA teams (common implementation efforts currently conducted by sales, go to market or customer success teams are not impacted by this policy).
- System Integration services including the creation of new API endpoints, custom flat files with scheduled tasks and nonstandard operations support requirements

If a third party is used by the customer for Systems Integration or Data Transformation services, those contracts should be between the customer and the vendor only.  There shall be no liability on ConstructConnect to support or troubleshoot this code, and ConstructConnect does not guarantee upgrade compatibility.

#### Policy Exception:
If a Policy Exception is required, a memo is required that includes the following artifacts:

- Business Justification and Current Opportunity Size
- Opportunity to resell the Custom Code Asset to other customers
- Custom Feature Functional Specification
- Custom Feature Development Estimate (hours)
- Base Product Inclusion – Timeline and additional development effort needed 
- Impact analysis to committed Product Roadmap – list of std. features delayed or deferred and length of deferment
- Time & Materials Statement of Work or Change Order (template will be provided) and provisions to support future Change Orders if needed.  It will be really, really, really, really hard to get an exception approved for Fixed-fee, Milestone based, or zero dollar SOW’s. 
- SOW or Change Order profitability including margin

##### This memo will be reviewed and approved by the following: 
- CXO, CTO, EVP Finance, VP Development, Team GM, and VP of IT Operations 
- Security review by CISO 
- Architecture Review Board
 
##### SOW or Change Order shall contain the following:
- Use of standard template (to be provided)
- Costs for additional development and testing environments if needed (cloud or managed hosting instances or DevOps costs to add in ConstructConnect data centers)
- Delivered through ConstructConnect standard Product Delivery Process through standard release management vehicles
- Payment Terms set to 30 days
- Warranty Period for custom code maximum length of 90 days after delivery
- Staffing Timeline (if new resources onboarded)
- Source code ownership for custom code resides with ConstructConnect and will not be transferred to the customer.
