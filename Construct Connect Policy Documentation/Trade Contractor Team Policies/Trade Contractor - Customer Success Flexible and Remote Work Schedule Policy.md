title: Trade Contractor - Customer Success Flexible and Remote Work Schedule Policy
published: 2019-09-11
author: Jim Hill - EVP and GM Trade Contractor

**Trade Contractor Customer Success Flexible and Remote Work Schedule Ploicy**

The company is committed to a work environment that accommodates our
People First/Family First values. Each team offers a flexible
schedule/telecommuting policy based on business sustainability and
individual performance. The following basic requirements must be met for
all teams:

-   Team members must have satisfactory attendance and cannot be on a
    Performance Improvement Plan

-   The work week for all full-time regular team members is 40 hours per
    week and 8 hours per day

-   Flexible/telecommuting work schedules for non-exempt team members
    should include an unpaid meal period of 30 minutes at minimum

-   There will be times when team members are required to depart from
    the flexible/telecommuting work schedule to accommodate changing
    situations and staffing needs. Reasonable notice will be provided
    when possible

-   The day before a holiday falling on a work day is treated as a
    normal 8 hour business day.

In addition to the basic requirements required for all teams by the
company the **Trade Contractor Customer Success** policy is
outlined below:

**Early Release Friday**

-   Team members may leave 1.5 hours earlier than their normal schedule
    as long as individual performance goals are being met and 6.5 hours
    are worked for the day. The exception to this would be your assigned
    day to cover customer calls until 5pm EST.

-   Team members on a Performance Improvement Plan are excluded from
    this benefit.

-   Managers reserve the right to ask team members to stay for their
    entire schedule due to business needs.

-   Early release and PTO cannot be combined. If PTO is taken on a
    Friday the entire day must be coded as PTO (8 hours). If a partial
    day is taken, PTO and hours worked must equal 8 hours. Lunch can be
    taken, but 6.5 hours should be worked that day and if salaried
    non-exempt, recorded as such in UltiPro.

**Schedule Changes and Telecommuting**

-   Full-time schedules outside of the core hours of 8 am -- 5 pm are
    available at the discretion of the manager.

-   Changes in schedule (arriving late, leaving early, telecommuting)
    may be approved for extenuating situations such as personal medical
    appointments, family-related issues, etc. at the discretion of the
    Manager.

-   Up to two hours may be made up in a week without using PTO.

-   We allow up to 1 telecommuting day per quarter at the discretion of
    the manager, provided that the team member is meeting the
    performance standards of their role. Additional days may be approved
    based on extenuating circumstances such as illness or injury, and
    must be approved at both the Director & VP level. A 24 hour notice
    must be given for this benefit. Exceptions may be approved based on
    extenuating circumstances.

-   Should a work from home day be approved, the team member must have
    full availability for the hours designated. All access to necessary
    systems must be in place.

-   Should a team member not meet the requirements or commitments to
    performance expectations, no future work from home days will be
    approved for one year from the date of the occurrence. Disciplinary
    actions may apply.

Team member acknowledges that they have read and understand the terms of
this agreement.
