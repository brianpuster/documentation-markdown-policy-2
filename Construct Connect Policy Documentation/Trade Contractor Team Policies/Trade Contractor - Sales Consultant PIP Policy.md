title: Trade Contractor - Sales Consultant PIP Policy
published: 2019-09-11
author: Jim Hill - EVP and GM Trade Contractor

**Mid-Market Sales Consultants**

**Performance Improvement Plan Process**

**Effective 10/11/2018**

Sales Consultants' performance will be reviewed by Sales Management on a
monthly basis at the beginning of each month for the previous month's
performance against the individual's established minimum monthly
threshold. If a sales consultant does not meet their full threshold, the
performance improvement plan process will begin as outlined below. Sales
Consultants who have not yet completed the 90-day orientation period
will not be subject to this process, but will be held responsible for a
successful orientation period, which consists of:

**Orientation Period Performance Guidelines**

-   Satisfactory attendance -- outlined in the Employment Handbook

-   Attitude that is open coaching and feedback

-   Achieve a minimum of 60% of the ACV ramp goal each month

*If team members are unable to demonstrate their ability to achieve a
satisfactory level of performance during their orientation period, their
employment could be terminated.*

*Upon successful completion of the orientation period team members will
enter the "regular" employment classification.*

**Performance Improvement Process**

-   Achieved below 60% of quota for two consecutive months: 1^st^ Stage
    Improvement Plan

-   Achieved below 60% for a rolling three-month period: 2^nd^ Stage
    Improvement Plan. Missed goal below 50%: up to and including
    termination.

    Or:

-   Achieved below 50% of quota for one month: 1^st^ Stage Improvement
    Plan

-   Achieved below 50% of quota for two consecutive months: 2^nd^ Stage
    Improvement Plan and up to termination.

-   While on 2^nd^ Stage Improvement Plan you must hit at minimum an
    average of 65% of quota for rolling three month period. Failure to
    do so will result in termination.

-   Stages run consecutively.

-   In order to be removed from performance improvement plan you must
    maintain 75% of goal for rolling three-month period for two
    consecutive months.

Management reserves the right to by-pass stages of the improvement
process depending on the nature of the issue.

Minimum monthly threshhold as it pertains to the performance plan will
be pro-rated for approved FMLA or bereavement absences.

Sales Consultants' with planned time off (PTO time) scheduled and
approved at least a month in advance for a minimum of 3 consecutive days
will only be responsible for the pro-rata portion of the goal associated
with the number of business days worked. 

This is a guideline for a performance improvement plan and is subject to
change at the discretion of the VP/GM Trade Contractors or CPO of
ConstructConnect.

I have read and acknowledge the Mid-Market Sales Consultant Performance
Improvement Plan Process to be the process I will be held accountable to
regarding my performance.
