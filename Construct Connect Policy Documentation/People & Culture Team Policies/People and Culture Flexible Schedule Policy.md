title: People & Culture -  FLEXIBLE SCHEDULE POLICY
published: 2019-07-18
author: Julie Storm - Chief People Officer



The company is committed to a work environment that accommodates our
People First/Family First values. Each team offers a
flexible/telecommuting schedule based on business sustainability and
individual performance. The following basic requirements must be met for
all teams:

-   Team members must have a satisfactory attendance and performance
    record

-   The work week for all full-time regular team members is 40 hour per
    week and 8 hours per day

-   Flexible/telecommuting work schedules for non-exempt team members
    should include an unpaid meal period of 30 minutes at minimum

-   There will be times when team members are required to depart from
    the flexible/telecommuting work schedule to accommodate changing
    situations and staffing needs. Reasonable notice will be provided
    when possible

In addition to the basic requirements required for all teams by the
company, the **People and Culture** policy is outlined
below:

# Early Release

-   Team members may leave 1.5 hours earlier than their normal schedule
    as long as individual performance goals are being met and 6.5 hours
    are worked for the day.

-   The CPO reserves the right to ask team members to stay for their
    entire schedule due to business needs.

-   Early release and PTO cannot be combined

# Telecommuting

-   Our team offers the opportunity to work from home on a pre-arranged
    basis for the following reasons:

-   The need to work on a major project that requires focus and a quiet
    environment

-   To work around personal medical or family-related appointments

-   Inclement weather that prohibits the ability to travel to the office

-   Emergencies due to home-related issues such as loss of power,
    flooding, etc.

# Flexible Scheduling

The standard hours for ConstructConnect are 8:00 am -- 5:00 pm. The
People and Culture Team allows formal flexible schedules outside of
these hours at the discretion of the manager as long as 40 hours are
worked in the work week.

Team Member Name: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Flextime Schedule: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Reason: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Date Schedule Begins: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Date Schedule Ends (if applicable): \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_



Team member acknowledges that they have read and understand the terms of
this agreement.

Team Member \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ Date:
\_\_\_\_\_\_\_\_\_\_\_\_

Manager \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ Date:
\_\_\_\_\_\_\_\_\_\_\_\_